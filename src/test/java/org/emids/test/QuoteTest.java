package org.emids.test;

import static org.junit.Assert.assertEquals;

import org.emids.model.UserBasicInformation;
import org.emids.model.UserHabit;
import org.emids.model.UserHealth;
import org.emids.service.InsuranceQuoteService;
import org.junit.Test;

public class QuoteTest {
	
	@Test
	public void getQuote(){
		
		InsuranceQuoteService iqs = new InsuranceQuoteService();
		
		UserBasicInformation userData = new UserBasicInformation();	
		
		userData.setName("Nitesh");
		userData.setGender("Male");
		userData.setAge("34");
		
		//HEALTH
		UserHealth health = new UserHealth();
		health.setHypertension(false);
		health.setBlodPresure(false);
		health.setOverWeight(true);
		health.setBlodSuger(false);
		userData.setUserHealth(health);
		
		//HABIT
		UserHabit habit = new UserHabit();
		habit.setAlcohal("y");
		habit.setDailyExercise("y");
		habit.setDrugs("n");
		habit.setSmoking("n");
		userData.setUserHabit(habit);
		
		String result=iqs.getQuote(userData);
		assertEquals(result, "Health Insurance Premium for Mr. Nitesh: Rs.6,856");
		System.out.println(result);
		
	}

}

<html lang="en" class="">
<head>
<meta charset="UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
	<h2>Health Insurance Premium Quote generator</h2>
	<form action="" method="post" id="contact_form">
		Name : <input name="name" placeholder="Name" type="text"> <br />
		<br /> Gender: <input type="radio" name="gender" value="Male" /> Male
		<input type="radio" name="gender" value="Female" /> Female <input
			type="radio" name="gender" value="Other" /> Other <br />
		<br /> Age: <input type="number" name="age" size="3" /> Years <br />
		<h4>Current health:</h4>
		Hyper Tension: <select name="hypertension">
			<option value="">Please select</option>
			<option value="true">Yes</option>
			<option value="false">No</option>
		</select> <br />
		<br /> Blood Suger: <select name="blood_suger">
			<option value=" ">Please select</option>
			<option value="true">Yes</option>
			<option value="false">No</option>
		</select> <br />
		<br /> Overweight: <select name="overweight">
			<option value=" ">Please select</option>
			<option value="true">Yes</option>
			<option value="false">No</option>
		</select> <br />
		<br /> Blood Pressure: <select name="blood_presure">
			<option value=" ">Please select</option>
			<option value="true">Yes</option>
			<option value="false">No</option>
		</select> <br />
		<h4>Current habit:</h4>
		Alcohol: <select name="alcohal">
			<option value=" ">Please select</option>
			<option value="y">Yes</option>
			<option value="n">No</option>
		</select> <br />
		<br /> Daily Exercise: <select name="dailyexercise">
			<option value=" ">Please select</option>
			<option value="y">Yes</option>
			<option value="n">No</option>
		</select> <br />
		<br /> Drugs: <select name="drugs">
			<option value=" ">Please select</option>
			<option value="y">Yes</option>
			<option value="n">No</option>
		</select> <br />
		<br /> Smoking: <select name="smoking">
			<option value=" ">Please select</option>
			<option value="y">Yes</option>
			<option value="n">No</option>
		</select> <br />
		<br />
		<!-- Success message -->

		<button type="submit" id="submit">Send</button>
		<div class="alert alert-success" role="alert" id="success_message"
			style="text-align: center">
			<i class="glyphicon glyphicon-thumbs-up"></i>
		</div>
		<script>
			$("#submit")
					.click(
							function(event) {
								//alert("clicked");
								var name = document.getElementsByName("name")[0].value;
								var age = document.getElementsByName("age")[0].value;
								var gender = document
										.getElementsByName("gender")[0].value;
								var hypertension = document
										.getElementsByName("hypertension")[0].value;
								var blood_suger = document
										.getElementsByName("blood_suger")[0].value;
								var overweight = document
										.getElementsByName("overweight")[0].value;
								var alcohal = document
										.getElementsByName("alcohal")[0].value;
								var dailyexercise = document
										.getElementsByName("dailyexercise")[0].value;
								var drugs = document.getElementsByName("drugs")[0].value;
								var blood_presure = document
										.getElementsByName("blood_presure")[0].value;
								var smoking = document
										.getElementsByName("smoking")[0].value;
								var jsonbody = {
									"name" : name,
									"gender" : gender,
									"age" : age,
									"userHealth" : {
										"hypertension" : hypertension,
										"blodSuger" : blood_suger,
										"overWeight" : overweight,
										"blodPresure" : blood_presure
									},
									"userHabit" : {
										"alcohal" : alcohal,
										"dailyExercise" : dailyexercise,
										"smoking" : smoking,
										"drugs" : drugs
									}
								}
								//alert(JSON.stringify(jsonbody));
								$
										.ajax({
											headers : {
												'Accept' : 'application/json',
												'Content-Type' : 'application/json',
											},
											type : "POST",
											dataType : "json",
											data : JSON.stringify(jsonbody),
											url : "http://localhost:2080/insurance-premium/quote",
											success : function(data) {
												if (data) {
													console.log(data);
													console.log(data.result);
													$('#success_message')
															.show();
													$('#success_message').html(
															data.result);
												}
											},
											error : function(e) {
												console.log(e);
											}
										});
								event.preventDefault();
							});
		</script>
	</form>
</body>
</html>

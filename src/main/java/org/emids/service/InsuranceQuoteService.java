package org.emids.service;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.emids.Util.AgeCount;
import org.emids.Util.IConstantsUtil;
import org.emids.model.UserBasicInformation;
import org.emids.model.UserHealth;
import org.springframework.stereotype.Service;

@Service("quoteService")
public class InsuranceQuoteService implements IQuoteService {

	public String getQuote(UserBasicInformation userData) {
		String result = null;

		try {

			double premium = 5000;

			String name = userData.getName();
			String gender = userData.getGender();
			int age = Integer.parseInt(userData.getAge());
			// HEALTH DATA
			/*String hypertension = userData.getUserHealth().getHypertension();
			String blod_presure = userData.getUserHealth().getBlodPresure();
			String blod_suger = userData.getUserHealth().getBlodSuger();
			String overweight = userData.getUserHealth().getOverWeight();*/
			// HABIT DATA
			String smoking = userData.getUserHabit().getSmoking();
			String alcohal = userData.getUserHabit().getAlcohal();
			String dailyexercise = userData.getUserHabit().getDailyExercise();
			String drugs = userData.getUserHabit().getDrugs();

			int agecnt = AgeCount.ageCount(age);

			double percentageAccordingToAge = 1.0;
			for(int i = 1; i<agecnt; i++){
				percentageAccordingToAge = percentageAccordingToAge * (1 + .10);
			}
			premium = premium *percentageAccordingToAge;
			
			if (age >= 40) {
				int agediff = age - 40;
				int periodOf5 = (agediff / 5);
				for (int i = 0; i < periodOf5; i++) {
					premium += premium * .2;
				}
			}
			if (gender.equals(IConstantsUtil.MALE)) {
				premium += premium * .02;
			}

			// HEALTH	
			int c = userData.getUserHealth().getFieldValue();
			System.out.println("C : "+c);
			premium += (premium * .01)*c;
			
			
			/*if (hypertension.equalsIgnoreCase(IConstantsUtil.YES)) {
				premium += premium * .01;
			}
			if (blod_suger.equalsIgnoreCase(IConstantsUtil.YES)) {
				premium += premium * .01;
			}
			if (overweight.equalsIgnoreCase(IConstantsUtil.YES)) {
				premium += premium * .01;
			}
			if (blod_presure.equalsIgnoreCase(IConstantsUtil.YES)) {
				premium += premium * .01;
			}*/
			
			// HABIT
			double total =0;
			if (smoking.equalsIgnoreCase(IConstantsUtil.YES)) {
				total = total + premium * .03;
			}

			if (alcohal.equalsIgnoreCase(IConstantsUtil.YES)) {
				total = total + premium * .03;
			}

			if (drugs.equalsIgnoreCase(IConstantsUtil.YES)) {
				total = total + premium * .03;
			}

			if (dailyexercise.equalsIgnoreCase(IConstantsUtil.YES)) {
				total = total - premium * .03;
			}
			premium = premium+total;
			
			NumberFormat formatter = new DecimalFormat("#,###");  
			result = "Health Insurance Premium for Mr. " + name + ": Rs." +formatter.format(Math.ceil(premium));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}

}

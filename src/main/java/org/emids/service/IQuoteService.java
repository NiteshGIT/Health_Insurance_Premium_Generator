package org.emids.service;

import org.emids.model.UserBasicInformation;


public interface IQuoteService {
	
	public String getQuote(UserBasicInformation userData);

}

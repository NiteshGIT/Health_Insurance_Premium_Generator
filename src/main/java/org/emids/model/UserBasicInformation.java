package org.emids.model;

public class UserBasicInformation {

	private String name;
	private String gender;
	private String age;
	private UserHealth userHealth;
	private UserHabit userHabit;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public UserHealth getUserHealth() {
		return userHealth;
	}

	public void setUserHealth(UserHealth userHealth) {
		this.userHealth = userHealth;
	}

	public UserHabit getUserHabit() {
		return userHabit;
	}

	public void setUserHabit(UserHabit userHabit) {
		this.userHabit = userHabit;
	}

	

}

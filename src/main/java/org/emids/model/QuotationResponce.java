package org.emids.model;

public class QuotationResponce {

	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "QuotationResponce [result=" + result + "]";
	}

}

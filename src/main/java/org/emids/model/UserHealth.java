package org.emids.model;

public class UserHealth {
	private boolean hypertension;
	private boolean blodPresure;
	private boolean blodSuger;
	private boolean overWeight;

	public boolean isHypertension() {
		return hypertension;
	}

	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}

	public boolean isBlodPresure() {
		return blodPresure;
	}

	public void setBlodPresure(boolean blodPresure) {
		this.blodPresure = blodPresure;
	}

	public boolean isBlodSuger() {
		return blodSuger;
	}

	public void setBlodSuger(boolean blodSuger) {
		this.blodSuger = blodSuger;
	}

	public boolean isOverWeight() {
		return overWeight;
	}

	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}

	public int getFieldValue() {
		int yCount = 0;
		if (this.isHypertension()) {
			yCount++;
		}
		if (this.isBlodPresure()) {
			yCount++;
		}
		if (this.isBlodSuger()) {
			yCount++;
		}
		if (this.isOverWeight()) {
			yCount++;
		}
		return yCount;
	}

}

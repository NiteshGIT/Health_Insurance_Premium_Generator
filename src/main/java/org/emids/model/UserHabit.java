package org.emids.model;

public class UserHabit {

	private String smoking;
	private String alcohal;
	private String dailyExercise;
	private String drugs;
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohal() {
		return alcohal;
	}
	public void setAlcohal(String alcohal) {
		this.alcohal = alcohal;
	}
	public String getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	
}

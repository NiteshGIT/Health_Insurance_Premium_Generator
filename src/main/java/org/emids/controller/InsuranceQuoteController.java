package org.emids.controller;

import org.emids.model.QuotationResponce;
import org.emids.model.UserBasicInformation;
import org.emids.service.IQuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class InsuranceQuoteController {

	@Autowired
	IQuoteService quoteService;
	
	@RequestMapping(value = "/quote", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public QuotationResponce generoteQuote(@RequestBody UserBasicInformation user){
		QuotationResponce quotationResponce = new QuotationResponce();
		try{
			if(user != null){
				String quotation = quoteService.getQuote(user);
				quotationResponce.setResult(quotation);
			}
		}catch(Exception e){
			quotationResponce.setResult("Can not get quote : Internal server error");
			e.printStackTrace();
			e.getMessage();
		}
		
		return quotationResponce;
	}
}

package org.emids.Util;

public class AgeCount {
	
	public static int ageCount(int age){
		int agecnt = 0;
		if (age < 18) {
			agecnt = 1;
		}
		if (age >= 18 && age < 25) {
			agecnt = 2;
		}
		if (age >= 25 && age < 30) {
			agecnt = 3;
		}
		if (age >= 30 && age < 35) {
			agecnt = 4;
		}
		if (age >= 35 && age < 40) {
			agecnt = 5;
		}
		if (age >= 40) {
			agecnt = 6;
		}
		return agecnt;
	}

}
